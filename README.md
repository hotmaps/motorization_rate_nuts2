# motorization_rate_nuts2


## Repository structure
```
data                    -- containts the dataset in CSV format
readme.md               -- Readme file 
datapackage.json        -- Includes the meta information of the dataset for processing and data integration

```


## Documentation

This dataset of motorization rates by NUTS2 was obtained from the study of Eurostat concerning the motorization rates [1] at NUTS2 level, until 2021. The dataset is available on the Eurostat Data Browser [2].


### Limitations of the dataset
Data were not available for some NUTS2. If the motorization rate for the year 2021 was not available, the lates available motorization rate was take as input. The data wa missing for the following NUTS2 :  PT11, PT15, PT16, PT17, PT18, PT2, PT3, NO0B



### References 
[1] [Eurostat Study](https://ec.europa.eu/eurostat/web/products-eurostat-news/w/DDN-20230530-1)

[2] [Dataset](https://ec.europa.eu/eurostat/databrowser/view/TRAN_R_VEHST__custom_6386000/bookmark/table?lang=en&bookmarkId=dfe12002-a708-49eb-ab45-cc56cec7fe4f)



## How to cite
Jeannin Noémie, OPENGIS4ET Project WP3


## Authors
Jeannin Noémie <sup>*</sup>

<sup>*</sup> [EPFL, PV-Lab](https://www.epfl.ch/labs/pvlab/), Laboratory of photovoltaics and thin-films electronics, Rue de la Maladière 71b, CH-2002 Neuchâtel 2


## License
Copyright © 2023: Noémie Jeannin
 
Creative Commons Attribution 4.0 International License
This work is licensed under a Creative Commons CC BY 4.0 International License.

SPDX-License-Identifier: CC-BY-4.0

License-Text: https://spdx.org/licenses/CC-BY-4.0.html

## Acknowledgment
We would like to convey our deepest appreciation to the OPENGIS4ET Project, which provided the funding to carry out the present investigation.

